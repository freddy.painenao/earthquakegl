package com.earthquakes.controller;

import java.text.ParseException;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.earthquakes.dto.EarthquakesCountryRequest;
import com.earthquakes.dto.EarthquakesMagnitudRequest;
import com.earthquakes.dto.EarthquakesMultipleCountryDatesRequest;
import com.earthquakes.dto.EarthquakesMultipleDatesRequest;
import com.earthquakes.dto.EarthquakesRequest;
import com.earthquakes.dto.EarthquakesResponse;
import com.earthquakes.dto.GenericDtoResponse;
import com.earthquakes.service.EarthquakesMagnitudService;
import com.earthquakes.service.EarthquakesSismosService;
import com.earthquakes.utils.Constants;
import com.earthquakes.utils.InfoEnumUtil;
import com.earthquakes.utils.LogMessageUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase controlador para exponer metodo de Sismos de Api Earthquakes
 * 
 * @author fpainenao
 * 
 */
@RestController
@Api(value = "EarthquakesController")
public class EarthquakesController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(EarthquakesController.class);

	@Autowired
	private EarthquakesSismosService earthquakesSismosService;

	@Autowired
	private EarthquakesMagnitudService earthquakesMagnitudService;

	/**
	 * Metodo que lista de sismo de acuerdo a un rango de fecha
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Consulta sismo por rango de Fechas")
	@PostMapping("/consulta/sismo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = " Consulta sismo �xito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> rangoFecha(@RequestBody EarthquakesRequest request)
			throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_RANGOFECHA.getCode(),
				"/consulta/sismos", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		EarthquakesResponse lDtor = earthquakesSismosService.consultaRangoFecha(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SISMO_RANGOFECHA.getCode(),
				"/consulta/sismos", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

	/**
	 * Metodo que obtiene lista de sismo apartir de un rango de magnitud
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Consulta Sismo Magnitud")
	@PostMapping("/consulta/magnitud")
	@ApiResponses(value = { @ApiResponse(code = 200, message = " Consulta magnitud sismo �xito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> escala(@RequestBody EarthquakesMagnitudRequest request)
			throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		EarthquakesResponse lDtor = earthquakesMagnitudService.consultaMagnitud(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

	/**
	 * Metodo que lista de sismo de acuerdo a un rango de fecha
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Consulta sismo por rango de Fechas Async")
	@PostMapping("/consulta/sismo_rango_fechas")
	@ApiResponses(value = { @ApiResponse(code = 200, message = " Consulta sismo de acuerdo a 4 fechas �xito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> rangoFechasAsync(@RequestBody EarthquakesMultipleDatesRequest request)
			throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_RANGOFECHA_ASYNC.getCode(),
				"/consulta/rangoFechasAsync", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		CompletableFuture<EarthquakesResponse> lDtorR1 = earthquakesSismosService.consultaRangoHilo(request,
				Constants.RANGO_FECHA_R1);
		CompletableFuture<EarthquakesResponse> lDtorR2 = earthquakesSismosService.consultaRangoHilo(request,
				Constants.RANGO_FECHA_R2);
		Stream<Object> combined = Stream.of(lDtorR1, lDtorR2).map(CompletableFuture::join);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT,
				InfoEnumUtil.FIN_CONSULTA_SISMO_RANGOFECHA_ASYNC.getCode(), "/consulta/rangoFechasAsync",
				LogMessageUtil.BLANK, LogMessageUtil.BLANK);
		return this.returnSuccessHttp(combined);
	}

	/**
	 * Metodo consulta sismos por pais
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Consulta sismo por Pais")
	@PostMapping("/consulta/sismoPais/")
	@ApiResponses(value = { @ApiResponse(code = 200, message = " Consulta sismo Pais �xito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> pais(@RequestBody EarthquakesCountryRequest request)
			throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_PAIS.getCode(),
				"/consulta/sismos/pais", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		EarthquakesResponse lDtor = earthquakesSismosService.consultaSismoPais(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SISMO_PAIS.getCode(),
				"/consulta/sismos/pais", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

	/**
	 * Metodo Retorna el total de sismos apartir de dos filtros de busqueda
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Consulta sismo por rango de Fechas Async")
	@PostMapping("/consulta/sismoFechasPais")
	@ApiResponses(value = { @ApiResponse(code = 200, message = " Consulta sismo de acuerdo a 4 fechas �xito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> TotalAsync(@RequestBody EarthquakesMultipleCountryDatesRequest request)
			throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_RANGOFECHA_ASYNC.getCode(),
				"/consulta/sismoFechasPais", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		CompletableFuture<EarthquakesResponse> lDtorR1 = earthquakesSismosService.consultaTotalSismoHilo(request,
				Constants.RANGO_FECHA_R1);
		CompletableFuture<EarthquakesResponse> lDtorR2 = earthquakesSismosService.consultaTotalSismoHilo(request,
				Constants.RANGO_FECHA_R2);
		Stream<Object> combined = Stream.of(lDtorR1, lDtorR2).map(CompletableFuture::join);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT,
				InfoEnumUtil.FIN_CONSULTA_SISMO_RANGOFECHA_ASYNC.getCode(), "/consulta/sismoFechasPais",
				LogMessageUtil.BLANK, LogMessageUtil.BLANK);
		return this.returnSuccessHttp(combined);
	}

	/**
	 * Metodo que obtiene lista de sismo data una magnitud inicial y final
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Almacena Sismo Magnitud")
	@PostMapping("/guarda/magnitud")
	@ApiResponses(value = { @ApiResponse(code = 201, message = " Guarda magnitud sismo �xito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> saveMagnitud(@RequestBody EarthquakesMagnitudRequest request)
			throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		EarthquakesResponse lDtor = earthquakesMagnitudService.guardaMagnitud(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

}
