package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Properties {
	
	@JsonProperty("mag")
	 private double mag;
	@JsonProperty("place")
	private String place;
	@JsonProperty("tim")
	private long tim;
	@JsonProperty("updated")
	private long updated;
	@JsonProperty("tz")
	private  int tz;
	@JsonProperty("url")
	private String url;
	@JsonProperty("detail")
	private String detail;
	@JsonProperty("felt")
	private String felt;
	@JsonProperty("cdi")
	private String cdi;
	@JsonProperty("mmi")
	private String mmi;
	@JsonProperty("alert")
	private String alert;
	@JsonProperty("status")
	private String status;
	@JsonProperty("tsunami")
	private int tsunami;
	@JsonProperty("sig")
	private int sig;
	@JsonProperty("net")
	private String net;
	@JsonProperty("code")
	private String code;
	@JsonProperty("ids")
	private  String ids;
	@JsonProperty("sources")
	private String sources;
	@JsonProperty("types")
	private String types;
	@JsonProperty("nst")
	private int nst;
	@JsonProperty("dmin")
	private double dmin;
	@JsonProperty("rms")
	private double rms;
	@JsonProperty("gap")
	private String gap;
	@JsonProperty("magType")
	private String magType;
	@JsonProperty("type")
	private String type;
	@JsonProperty("title")
	private String title;

}
