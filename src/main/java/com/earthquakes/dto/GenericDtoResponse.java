package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class GenericDtoResponse {

	private Object result;

	@JsonInclude(Include.NON_NULL)
	private Object payload;

	public GenericDtoResponse() {
		super();
	}

	public GenericDtoResponse(Object result) {
		super();
		this.result = result;
	}

	public GenericDtoResponse(Object payload, String message) {
		super();
		this.payload = payload;
		this.result = new MsgResultResponse(message);
	}

	public Object getResult() {
		return result;
	}

	public Object getPayload() {
		return payload;
	}

	public class MsgResultResponse {

		@JsonInclude(Include.NON_NULL)
		private String message;

		public MsgResultResponse(String message) {
			super();
			this.message = message;
		}

		public String getMessage() {
			return message;
		}

	}

}

