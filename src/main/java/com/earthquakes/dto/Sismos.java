package com.earthquakes.dto;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Sismos {
	
	@JsonProperty("metadata")
	private Metadata metadata;
	@JsonProperty("type")
	private String type;
	@JsonProperty("features")
	private List<Features>features;
	

}
