package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EarthquakesCountryRequest {

	@JsonProperty("country")
	private String country;

}
