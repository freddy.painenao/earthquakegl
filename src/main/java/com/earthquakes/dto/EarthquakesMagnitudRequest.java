package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "minmagnitude", "maxmagnitude"})
@Getter
@Setter
@ToString
public class EarthquakesMagnitudRequest {

	@JsonProperty("minmagnitude")
	private double minMagnitude;
	@JsonProperty("maxmagnitude")
	private double maxMagnitude;
	
	
	

}
