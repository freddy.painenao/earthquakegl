package com.earthquakes.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EarthquakesResponse implements Serializable {

	private static final long serialVersionUID = -6207626164658348209L;

	@JsonProperty("status")
	private Integer status;

	@JsonProperty("message")
	private String message;
		
	@JsonProperty("data")
	private transient Object data;

	public EarthquakesResponse(Integer status, String message, Object data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public EarthquakesResponse() {
	}
	
}
