package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "starttime", "endtime"})
@Getter
@Setter
@ToString
public class EarthquakesMultipleDatesRequest {

	@JsonProperty("starttimeR1")
	private String startTimeR1;
	@JsonProperty("endtimeR1")
	private String endTimeR1;
	
	@JsonProperty("starttimeR2")
	private String startTimeR2;
	@JsonProperty("endtimeR2")
	private String endTimeR2;
	
	

}
