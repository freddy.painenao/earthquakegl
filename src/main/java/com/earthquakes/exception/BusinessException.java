package com.earthquakes.exception;

import com.earthquakes.utils.ErrorEnumUtil;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String errorCode;
	private final String errorMsg;

	public BusinessException(ErrorEnumUtil code) {
		this.errorMsg = code.getMsg();
		this.errorCode = code.getCode();
	}
	
	public BusinessException(ErrorEnumUtil code, String v) {
		this.errorMsg = String.format(code.getMsg(), v);
		this.errorCode = code.getCode();
	}
	
	public BusinessException(String modulo, String mensaje) {
		this.errorMsg = mensaje;
		this.errorCode = modulo;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}
}
