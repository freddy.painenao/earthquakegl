package com.earthquakes.repository;


import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.earthquakes.entity.SismosEntity;

@Repository
@Transactional
public interface RegistraSismosRepository extends CrudRepository<SismosEntity, Long> {


}
