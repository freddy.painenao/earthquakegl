package com.earthquakes.service;

import java.text.ParseException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.earthquakes.dto.EarthquakesCountryRequest;
import com.earthquakes.dto.EarthquakesMultipleCountryDatesRequest;
import com.earthquakes.dto.EarthquakesMultipleDatesRequest;
import com.earthquakes.dto.EarthquakesRequest;
import com.earthquakes.dto.EarthquakesResponse;
import com.earthquakes.dto.Features;
import com.earthquakes.dto.Sismos;
import com.earthquakes.exception.BusinessException;
import com.earthquakes.utils.Constants;
import com.earthquakes.utils.ErrorEnumUtil;
import com.earthquakes.utils.InfoEnumUtil;
import com.earthquakes.utils.LogMessageUtil;

/**
 * Servicio consulta API Earthquakes
 * 
 * @author fpainenao
 *
 */

@Service
public class EarthquakesSismosService {

	private static final Logger logger = LoggerFactory.getLogger(EarthquakesSismosService.class);

	@Value("${url.service.earthquake}${path.service.earthquake.sismos}")
	private String uriServiceEarthquakeRangoFecha;

	/**
	 * consulta Sismo segun RangoFecha
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public EarthquakesResponse consultaRangoFecha(EarthquakesRequest request) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		ResponseEntity<Sismos> respEarthquakes = sismoRegistrados(request);

		if (respEarthquakes.getStatusCode() == HttpStatus.BAD_REQUEST) {
			throw new BusinessException(ErrorEnumUtil.WSBUK_EARTHQUAKES);
		}
		response.setMessage("Respuesta Exitosa");
		response.setStatus(Constants.SUCCESS);
		response.setData(respEarthquakes.getBody());

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(), response);

		return response;
	}

	/**
	 * Servicio Earthquakes
	 *
	 * @param EarthquakesRequest inputEarthquakes)
	 * @return ResponseEntity<Sismos>
	 */

	protected ResponseEntity<Sismos> sismoRegistrados(EarthquakesRequest inputEarthquakes) {

		RestTemplate restTemplate = new RestTemplate();
		String starttime = "&starttime=";
		String endtime = "&endtime=";

		try {

			starttime += inputEarthquakes.getStartTime();
			endtime += inputEarthquakes.getEndTime();

			ResponseEntity<Sismos> respEarthquakes = restTemplate
					.getForEntity(uriServiceEarthquakeRangoFecha + starttime + endtime, Sismos.class);

			if (respEarthquakes.getStatusCodeValue() == Constants.SUCCESS
					&& respEarthquakes.getBody().getMetadata() != null) {

				if (logger.isInfoEnabled()) {
					logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT,
							InfoEnumUtil.INI_CONSULTA_SERVICE_EARTHQUAKE.getCode(), respEarthquakes.toString());
				}
				return respEarthquakes;
			}

		} catch (HttpStatusCodeException e) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE_EARTHQUAKE.getCode(),
					e.getStatusCode().value());
			Sismos respEarthquakes = new Sismos();
			return new ResponseEntity<>(respEarthquakes, HttpStatus.BAD_REQUEST);
		}

		return null;

	}

	/**
	 * Servicio consultaRangoHilo
	 *
	 * @param EarthquakesMultipleDatesRequest requestMultipleDates)
	 * @return CompletableFuture<EarthquakesResponse>
	 */

	@Async
	public CompletableFuture<EarthquakesResponse> consultaRangoHilo(
			EarthquakesMultipleDatesRequest requestMultipleDates, String rango) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		switch (rango) {
		case Constants.RANGO_FECHA_R1:
			EarthquakesRequest request = new EarthquakesRequest(requestMultipleDates.getStartTimeR1(),
					requestMultipleDates.getEndTimeR1());
			response = consultaSismos(request);
			break;
		case Constants.RANGO_FECHA_R2:
			EarthquakesRequest request2 = new EarthquakesRequest(requestMultipleDates.getStartTimeR2(),
					requestMultipleDates.getEndTimeR2());
			response = consultaSismos(request2);
			break;
		default:

		}

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(), response);

		return CompletableFuture.completedFuture(response);
	}

	/**
	 * Servicio consultaSismos Metodo privado que consume servicio de sismos de
	 * Earthquakes
	 *
	 * @param EarthquakesRequest request
	 * @return EarthquakesResponse
	 */

	private EarthquakesResponse consultaSismos(EarthquakesRequest request) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();
		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		ResponseEntity<Sismos> respEarthquakes = sismoRegistrados(request);

		if (respEarthquakes.getStatusCode() == HttpStatus.BAD_REQUEST) {
			throw new BusinessException(ErrorEnumUtil.WSBUK_EARTHQUAKES);
		}
		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(),
				respEarthquakes);

		response.setMessage("Respuesta Exitosa");
		response.setStatus(Constants.SUCCESS);
		response.setData(respEarthquakes.getBody());
		return response;
	}

	/**
	 * consulta Sismo filtrando por pais *
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public EarthquakesResponse consultaSismoPais(EarthquakesCountryRequest request) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);
		List<Features> respEarthquakesPais = sismoAll(request).getBody().getFeatures().stream().filter(
				features -> features.getProperties().getPlace().contains(StringUtils.capitalize(request.getCountry())))
				.collect(Collectors.toList());

		response.setMessage("Respuesta Exitosa");
		response.setStatus(Constants.SUCCESS);
		response.setData(respEarthquakesPais);

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(), response);

		return response;
	}

	/**
	 * Servicio Earthquakes que retorna todos los sismos
	 *
	 * @param EarthquakesRequest inputEarthquakes)
	 * @return ResponseEntity<Sismos>
	 */
	protected ResponseEntity<Sismos> sismoAll(EarthquakesCountryRequest inputEarthquakes) {

		RestTemplate restTemplate = new RestTemplate();
		try {

			ResponseEntity<Sismos> respEarthquakes = restTemplate.getForEntity(uriServiceEarthquakeRangoFecha,
					Sismos.class);

			if (respEarthquakes.getStatusCodeValue() == Constants.SUCCESS
					&& respEarthquakes.getBody().getMetadata() != null) {

				if (logger.isInfoEnabled()) {
					logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT,
							InfoEnumUtil.INI_CONSULTA_SERVICE_EARTHQUAKE.getCode(), respEarthquakes.toString());
				}
				return respEarthquakes;
			}

		} catch (HttpStatusCodeException e) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE_EARTHQUAKE.getCode(),
					e.getStatusCode().value());
			Sismos respEarthquakes = new Sismos();
			return new ResponseEntity<>(respEarthquakes, HttpStatus.BAD_REQUEST);
		}

		return null;

	}

	/**
	 * Servicio consultaRangoHilo
	 *
	 * @param EarthquakesMultipleDatesRequest requestMultipleDates)
	 * @return CompletableFuture<EarthquakesResponse>
	 */

	@Async
	public CompletableFuture<EarthquakesResponse> consultaTotalSismoHilo(
			EarthquakesMultipleCountryDatesRequest requestMultipleCountryDates, String rango) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		switch (rango) {
		case Constants.RANGO_FECHA_R1:
			EarthquakesRequest request = new EarthquakesRequest(requestMultipleCountryDates.getStartTimeR1(),
					requestMultipleCountryDates.getEndTimeR1());
			response = consultaTotalSismos(request, requestMultipleCountryDates.getCountryR1());
			break;
		case Constants.RANGO_FECHA_R2:
			EarthquakesRequest request2 = new EarthquakesRequest(requestMultipleCountryDates.getStartTimeR2(),
					requestMultipleCountryDates.getEndTimeR2());
			response = consultaTotalSismos(request2, requestMultipleCountryDates.getCountryR2());
			break;
		default:

		}

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(), response);

		return CompletableFuture.completedFuture(response);
	}

	/**
	 * Servicio consultaSismos Metodo privado que consume servicio de sismos de
	 * Earthquakes
	 *
	 * @param EarthquakesRequest request
	 * @return EarthquakesResponse
	 */

	private EarthquakesResponse consultaTotalSismos(EarthquakesRequest request, String pais) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();
		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		List<Features> respEarthquakesTotalPaisFecha = sismoRegistrados(request).getBody().getFeatures().stream()
				.filter(features -> features.getProperties().getPlace().contains(StringUtils.capitalize(pais)))
				.collect(Collectors.toList());

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(),
				respEarthquakesTotalPaisFecha);

		response.setMessage("Total de Registro: " + respEarthquakesTotalPaisFecha.size());
		response.setStatus(Constants.SUCCESS);
		response.setData(respEarthquakesTotalPaisFecha);
		return response;
	}

}
