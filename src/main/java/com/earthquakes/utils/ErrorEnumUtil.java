package com.earthquakes.utils;

import java.util.Map;
import com.google.common.collect.Maps;

public enum ErrorEnumUtil {
	
	WSBUK_EARTHQUAKES("WSBUK_EARTHQUAKES", "Error servicio EARTHQUAKES");
	
	private final String code;
    private final String msg;

    ErrorEnumUtil(String code, String msg) {
	this.code = code;
	this.msg = msg;
    }

    public String getCode() {
	return this.code;
    }

    public String getMsg() {
	return this.msg;
    }

    private static final Map<String, String> nameIndex = Maps.newHashMapWithExpectedSize(ErrorEnumUtil.values().length);
    static {
	for (ErrorEnumUtil suit : ErrorEnumUtil.values()) {
	    nameIndex.put(suit.getCode(), suit.getMsg());
	}
    }

    public static String lookupByName(String name) {
	return nameIndex.get(name);
    }

}
