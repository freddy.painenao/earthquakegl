package com.earthquakes.utils;

import java.io.Serializable;

public class Constants implements Serializable {

	private static final long serialVersionUID = 6354630435599385052L;

	public Constants() {
		throw new IllegalStateException("Utility class");
	}

	public static final String FORMATO_FECHA_SB = "dd/MM/yyyy";
	public static final String FORMATO_FECHA_BUK = "yyyy-MM-dd";
	public static final String ESTADO_FINALIZADO = "FINALIZADO";
	public static final String RANGO_FECHA_R1 = "R1";
	public static final String RANGO_FECHA_R2 = "R2";
	public static final int SUCCESS = 200;
	public static final int CREATE_UPDATE = 201;
}
