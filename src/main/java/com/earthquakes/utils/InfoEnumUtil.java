package com.earthquakes.utils;

public enum InfoEnumUtil {

	INI_CONSULTA_SISMO_RANGOFECHA("001-INI CONTROLLER CONSULTA SISMOS CONTROLLER ",
			"Consulta Sismo segun rango de Fechas"),

	FIN_CONSULTA_SISMO_RANGOFECHA("002-FIN CONTROLLER CONSULTA SISMOS CONTROLLER ",
			"FIN Controller Consulta Sismo segun rango de Fechas"),

	INI_CONSULTA_SERVICE("003-INI SERVICE CONSULTA SISMOS CONTROLLER",
			"INI Services Consulta Sismo segun rango de Fechas"),

	FIN_CONSULTA_SERVICE("004-FIN SERVICE CONSULTA SISMOS CONTROLLER",
			"FIN Services Consulta Sismo segun rango de Fechas"),
	INI_CONSULTA_SERVICE_EARTHQUAKE("005-INI CONSULTA SERVICIO EARTHQUAKE", "INI consulta servicio EARTHQUAKE"),
	FIN_CONSULTA_SERVICE_EARTHQUAKE("006-INI CONSULTA SERVICIO EARTHQUAKE", "INI consulta servicio EARTHQUAKE"),

	INI_CONSULTA_SISMO_MAGNITUD("007-INI CONTROLLER CONSULTA MAGNITUD CONTROLLER ", "Consulta Magnitud Sismo"),

	FIN_CONSULTA_SISMO_MAGNITUD("008-FIN CONTROLLER CONSULTA MAGNITUD CONTROLLER ",
			"FIN Controller Consulta Magnitud Sismo"),

	INI_CONSULTA_SISMO_RANGOFECHA_ASYNC("009-INI CONTROLLER CONSULTA SISMOS CONTROLLER ASYNC ",
			"Consulta Sismo segun rango de Fechas Async"),

	FIN_CONSULTA_SISMO_RANGOFECHA_ASYNC("010-FIN CONTROLLER CONSULTA SISMOS CONTROLLER ASYNC ",
			"FIN Controller Consulta Sismo segun rango de Fechas Async"),

	INI_CONSULTA_SISMO_PAIS("011-INI CONTROLLER CONSULTA SISMOS CONTROLLER PAIS ", "Consulta Sismo Pais"),

	FIN_CONSULTA_SISMO_PAIS("012-FIN CONTROLLER CONSULTA SISMOS CONTROLLER PAIS ",
			"FIN Controller Consulta Sismo segun rango de Fechas pais");

	private final String code;
	private final String msg;

	InfoEnumUtil(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return this.code;
	}

	public String getMsg() {
		return this.msg;
	}
}
