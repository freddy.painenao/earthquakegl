package com.earthquakes.utils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils implements Serializable {

	private static final long serialVersionUID = 6808461961003758547L;

	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	public Utils() {
		throw new IllegalStateException("Utility class");
	}

	public static Date parseDateToDate(String fecha, String formato) {

		Date date = new Date();
		SimpleDateFormat format;
		try {
			format = new SimpleDateFormat(formato);
			format.setLenient(false);
			date = format.parse(fecha);
		} catch (Exception e) {
			logger.error("Fecha invalida enviada: {}", e.getMessage());
			date = null;
		}

		return date;
	}

	public static String parseDateToString(Date fecha, String formato) {

		String date = null;
		SimpleDateFormat format;
		try {
			format = new SimpleDateFormat(formato);
			format.setLenient(false);
			date = format.format(fecha);
		} catch (Exception ex) {
			logger.error("Fecha invalida enviada: {}", ex.getMessage());
			date = null;
		}

		return date;
	}
	
	
	public static <T> CompletableFuture<List<T>> allOf(List<CompletableFuture<T>> futuresList) {
	    CompletableFuture<Void> allFuturesResult =
	    CompletableFuture.allOf((CompletableFuture<?>[]) futuresList.toArray());
	    return allFuturesResult.thenApply(v ->
	            futuresList.stream().
	                    map(future -> future.join()).
	                    collect(Collectors.<T>toList())
	    );
	}
}
