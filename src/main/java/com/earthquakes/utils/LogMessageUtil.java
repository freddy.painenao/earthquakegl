package com.earthquakes.utils;


public final class LogMessageUtil {

	private LogMessageUtil() {
	}

	// Controller
	public static final String DEBUG_PARAMETROS_ENTRADA_CONTROLLER = "Debug Controller - PARAMETROS ENTRADA: {}";
	public static final String DEBUG_PARAMETROS_SALIDA_CONTROLLER = "Debug Controller - PARAMETROS SALIDA: {} - STATUS: {}";
	public static final String STATUS_OK = "Servicio finalizado con �xito";
	

	//BUK
	public static final String DEBUG_PARAMETROS_ENTRADA = "Debug Soap Client - PARAMETROS ENTRADA: {}";
	public static final String ERROR_BUK_CALL = "Error en la llamada BUK - Codigo: {} - Mensaje: {}";
	public static final String ERROR_BUK_ANSWER = "Error en la respuesta BUK - Codigo: {} - Mensaje: {}";

	//Error
	public static final String ERROR = "Error: C�digo:{} : Mensaje: {}";

	// Exito sin parametros
	public static final String SIN_PARAMETROS_SALIDA = "Sin par�metros de salida";
	
	public static final String INPUT_VACIO = "Sin par�metro";

	
	public static final String BLANK = "";
	
	// V2 Logs
	public static final String NAMESPACE = "FIDCL-APILOY:";
	
	public static final String V2_INFO_CONTROLLER_INPUT = LogMessageUtil.NAMESPACE + "CONTROLLER:INPUT:{}:{}:{}:{}";
	public static final String V_INFO_CONTROLLER_INPUT = LogMessageUtil.NAMESPACE + "CONTROLLER:INPUT:{}:{}:{}:{}";
	
	public static final String V2_INFO_CONTROLLER_OUTPUT = LogMessageUtil.NAMESPACE + "FIN CONTROLLER:OUTPUT:{}:{}:{}:{}";
	public static final String V2_INFO_SERVICE_OUTPUT = LogMessageUtil.NAMESPACE + "SERVICE:FIN OUTPUT:{}:{}";
	public static final String V2_INFO_SERVICE_INPUT = LogMessageUtil.NAMESPACE + "SERVICE:INPUT:{}:{}";
	
	
	public static final String V2_ERROR_SERVICE = LogMessageUtil.NAMESPACE + "SERVICE:ERROR:{}:{}:{}";
	public static final String V2_ERROR_CLIENT = LogMessageUtil.NAMESPACE + "CLIENT:ERROR:{}:{}:{}";
}
